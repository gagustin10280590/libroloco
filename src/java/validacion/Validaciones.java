/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package validacion;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Daftzero
 */
public class Validaciones {

    public boolean ValidaCantidad (String libroId, String cantidad) {

        boolean errorFlag = false;

        if (!libroId.isEmpty() && !cantidad.isEmpty()) {

            int i = -1;

            try {

                i = Integer.parseInt(cantidad);
            } catch (NumberFormatException nfe) {

                System.out.println("el usuario no ha puesto una cantidad adecuada");
            }

            if (i < 0 || i > 99) {

                errorFlag = true;
            }
        }

        return errorFlag;
    }

    public boolean ValidaFormulario(String nombre,
                                String email,
                                String telefono,
                                String direccion,
                                String cp,
                                String tarjNum,
                                HttpServletRequest request) {

        boolean errorFlag = false;
        boolean errorNom;
        boolean emailError;
        boolean telefonoError;
        boolean direccionError;
        boolean cpError;
        boolean tarjNumError;
        System.out.println("estoy en valida");
        if (nombre == null
                || nombre.equals("")
                || nombre.length() > 45) {
            errorFlag = true;
            errorNom = true;
            request.setAttribute("errorNom", errorNom);
            System.out.println("error en nombre");
        }
        if (email == null
                || email.equals("")
                || !email.contains("@")) {
            errorFlag = true;
            emailError = true;
            request.setAttribute("emailError", emailError);
            System.out.println("error en mail");
        }
        if (telefono == null
                || telefono.equals("")
                || telefono.length() < 10) {
            errorFlag = true;
            telefonoError = true;
            request.setAttribute("telefonoError", telefonoError);
            System.out.println("error en telefono");
        }
        if (direccion == null
                || direccion.equals("")
                || direccion.length() > 45) {
            errorFlag = true;
            direccionError = true;
            request.setAttribute("direccionError", direccionError);
            System.out.println("error en direccion");
        }
        if (cp == null|| cp.equals("")|| cp.length() > 6) {
            errorFlag = true;
            cpError = true;
            request.setAttribute("cpError", cpError);
            System.out.println("error en cp");
        }
        if (tarjNum == null
                || tarjNum.equals("")
                || tarjNum.length() > 19) {
            errorFlag = true;
            tarjNumError = true;
            request.setAttribute("tarjNumError", tarjNumError);
            System.out.println("error en tarjn");
        }

        return errorFlag;
    }

}