/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package carro;

import entidad.Libro;

/**
 *
 * @author Daftzero
 */
public class CarroItem {

    Libro libro;
    short cantidad;

    public CarroItem(Libro libro) {
        this.libro = libro;
        cantidad = 1;
    }

    public Libro getLibro() {
        return libro;
    }

    public short getCantidad() {
        return cantidad;
    }

    public void setCantidad(short cantidad) {
        this.cantidad = cantidad;
    }

    public void incrementaCantidad() {
        cantidad++;
    }

    public void reduceCantidad() {
        cantidad--;
    }

    public double getTotal() {
        double amount = 0;
        amount = (this.getCantidad() * libro.getPrecio().doubleValue());
        return amount;
    }

}