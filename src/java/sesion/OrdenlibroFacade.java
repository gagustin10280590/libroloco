/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Ordenlibro;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daftzero
 */
@Stateless
public class OrdenlibroFacade extends AbstractFacade<Ordenlibro> {
    @PersistenceContext(unitName = "LibroLocoPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrdenlibroFacade() {
        super(Ordenlibro.class);
    }
    
    public List<Ordenlibro> findByOrdenId(Object ordenId) {
    return em.createNamedQuery("Ordenlibro.findByOrdenId").setParameter("ordenId", ordenId).getResultList();    }

    
    
}
